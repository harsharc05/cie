import streamlit as st
import pandas as pd
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

# Load the model from disk
import joblib
from preprocessing import preprocess

# Load the model
model = joblib.load(r"./notebook/model.sav")

segment_descriptions = {
                    1: "Customers with a tenure of 20 months or less.",
                    2: "Customers with contracts other than a two-year contract.",
                    3: "Customers who are either not partners or have no dependents.",
                    4: "Customers with fiber optic internet service.",
                    5: "Customers with total charges less than or equal to $1000 or monthly charges greater than $75.",
                    6: "Customers who use electronic check as their payment method.",
                    7: "Customers without online security.",
                    8: "Customers without tech support.",
                    9: "Customers without device protection.",
                    10: "Customers without online backup."
                }
retention_strategies_dict = {
                1 : [
                    "Personalized Onboarding",
                    "Discounts for Longer Commitment",
                    "Educational Content",
                    "Customer Support Outreach",
                    "Exclusive Features",
                    "Loyalty Program",
                    "Feedback Surveys",
                    "Personalized Recommendations",
                    "Community Engagement",
                    "Regular Check-ins"
                ],
                2: [
                    "Contract Renewal Incentives",
                    "Personalized Contract Options",
                    "Value-Added Services",
                    "Flexible Terms",
                    "Early Renewal Discounts",
                    "Exclusive Packages",
                    "Personalized Communication",
                    "Customer Loyalty Program",
                    "Regular Account Reviews",
                    "Dedicated Account Manager"
                ],
                3: [
                    "Family Bundle Discounts",
                    "Refer-a-Friend Program",
                    "Exclusive Family Features",
                    "Customized Family Plans",
                    "Personalized Recommendations",
                    "Family Events or Promotions",
                    "Educational Materials",
                    "Flexible Billing Options",
                    "Customer Satisfaction Surveys",
                    "Community Building"
                ],
                4: [
                    "Performance Optimization",
                    "Upgrade Offers",
                    "Discounts for Bundled Services",
                    "Technical Support",
                    "Educational Workshops",
                    "Continuous Improvements",
                    "Customer Feedback Sessions",
                    "Competitive Pricing",
                    "Exclusive Fiber Optic Packages",
                    "Community Forums"
                ],
                5: [
                    "Flexible Payment Plans",
                    "Discounts for Bundle Services",
                    "Promotional Packages",
                    "Usage-Based Pricing",
                    "Financial Counseling",
                    "Customizable Plans",
                    "Loyalty Discounts",
                    "Referral Programs",
                    "Educational Resources",
                    "Customer Assistance Hotline"
                ],
                6: [
                    "Alternative Payment Options",
                    "Automatic Payment Plans",
                    "Payment Reminders",
                    "Online Payment Portals",
                    "Incentives for Other Methods",
                    "Education on Secure Payments",
                    "Customer Support Assistance",
                    "Exclusive Benefits for Other Methods",
                    "Online Tutorials",
                    "Feedback Gathering"
                ],
                7: [
                    "Security Package Offer",
                    "Educational Webinars",
                    "Discounted Security Add-ons",
                    "Free Trial Periods",
                    "Customizable Security Plans",
                    "Regular Security Updates",
                    "Customer Workshops",
                    "Referral Programs",
                    "24/7 Security Support",
                    "Security Awareness Campaigns"
                ],
                8: [
                    "Enhanced Tech Support Packages",
                    "24/7 Tech Support",
                    "Personalized Tech Solutions",
                    "Priority Response for Loyal Customers",
                    "Educational Resources",
                    "Tech Support Hotline",
                    "Customer Feedback Mechanism",
                    "Remote Assistance Services",
                    "Interactive Troubleshooting Tools",
                    "Discounts for Tech Support Add-ons"
                ],
                9: [
                    "Device Protection Package",
                    "Discounted Device Protection",
                    "Extended Warranty Programs",
                    "Device Upgrade Promotions",
                    "Educational Webinars",
                    "Customer Workshops",
                    "Device Security Software Inclusions",
                    "Referral Programs",
                    "Loyalty Discounts",
                    "Customer Support Hotline"
                ],
                10: [
                    "Comprehensive Backup Plans",
                    "Discounted Backup Services",
                    "Automatic Backup Activation",
                    "Data Recovery Promotions",
                    "Educational Materials",
                    "Customer Training Sessions",
                    "Referral Programs",
                    "Regular Backup Reminders",
                    "Enhanced Data Security",
                    "Loyalty Rewards"
                ]
            }

def main():
    # Setting Application title
    st.title('Customer Insight Engine')
    
    # Setting Application description
    st.markdown("""
      Our customer churn prediction model empowers telecom companies with advanced analytics 
                to proactively identify and retain customers at risk of churning. \n
    """)
    st.markdown("<h3></h3>", unsafe_allow_html=True)
    add_selectbox = st.sidebar.selectbox(
        "How would you like to predict?", ("Online", "Batch"))
    # Setting Application sidebar default
    image = Image.open('./Images/churn.jpeg')
    st.sidebar.info('Customer Churn prediction tool for telecom companies')
    st.sidebar.image(image)

    if add_selectbox == "Online":
        seniorcitizen = st.selectbox('Senior Citizen:', ('Yes', 'No'))
        dependents = st.selectbox('Dependent:', ('Yes', 'No'))
        partners = st.selectbox('Partner:', ('Yes', 'No'))
        tenure = st.slider('Number of months the customer has stayed with the company', min_value=0, max_value=72, value=0)
        paperlessbilling = st.selectbox('Paperless Billing', ('Yes', 'No'))
        monthlycharges = st.number_input('The amount charged to the customer monthly', min_value=0, max_value=150, value=0)
        totalcharges = st.number_input('The total amount charged to the customer', min_value=0, max_value=10000, value=0)
        contract = st.selectbox('Contract', ('Month-to-month', 'One year', 'Two year'))
        mutliplelines = st.selectbox("Does the customer have multiple lines", ('Yes', 'No', 'No phone service'))
        internetservice = st.selectbox("Does the customer have internet service", ('DSL', 'Fiber optic', 'No'))
        onlinesecurity = st.selectbox("Does the customer have online security", ('Yes', 'No', 'No internet service'))
        onlinebackup = st.selectbox("Does the customer have online backup", ('Yes', 'No', 'No internet service'))
        deviceprotection = st.selectbox("Does the customer have device protection", ('Yes', 'No', 'No internet service'))
        techsupport = st.selectbox("Does the customer have technology support", ('Yes', 'No', 'No internet service'))
        PaymentMethod = st.selectbox('PaymentMethod', ('Electronic check', 'Mailed check', 'Bank transfer (automatic)', 'Credit card (automatic)'))
        condition_1 = (tenure <= 20)
        condition_2 = contract != 'Two year'
        condition_3 = ((partners == 'No') | (dependents == 0))
        condition_4 = (internetservice == 'Fiber optic')
        condition_5 = ((totalcharges <= 1000) | (monthlycharges > 75))
        condition_6 = PaymentMethod == 'Electronic check'
        condition_7 = onlinesecurity == 'No'
        condition_8 = (techsupport == 'No')
        condition_9 = (deviceprotection == 'No')
        condition_10 = onlinebackup == 'No'
        if condition_1:
            seg = 1
        elif condition_2:
            seg = 2
        elif condition_3:
            seg = 3
        elif condition_4:
            seg = 4
        elif condition_5:
            seg = 5
        elif condition_6:
            seg = 6
        elif condition_7:
            seg = 7
        elif condition_8:
            seg = 8
        elif condition_9:
            seg = 9
        elif condition_10:
            seg = 10
        else:
            seg = 0

        data = {
            'SeniorCitizen': seniorcitizen,
            'Partner': partners,
            'Dependents': dependents,
            'tenure': tenure,
            'PaperlessBilling': paperlessbilling,
            'MonthlyCharges': monthlycharges,
            'TotalCharges': totalcharges,
            'Contract': contract,
            'MultipleLines': mutliplelines,
            'InternetService': internetservice,
            'OnlineSecurity': onlinesecurity,
            'OnlineBackup': onlinebackup,
            'DeviceProtection': deviceprotection,
            'TechSupport': techsupport,
            'PaymentMethod': PaymentMethod,
        }
        features_df = pd.DataFrame.from_dict([data])
        st.markdown("<h3></h3>", unsafe_allow_html=True)
        st.write('Overview of input is shown below ')
        st.markdown("<h3></h3>", unsafe_allow_html=True)
        st.dataframe(features_df)

        # Preprocess inputs
        preprocess_df = preprocess(features_df, 'Online')

        prediction = model.predict(preprocess_df)

        if st.button('Predict'):
            if prediction == 1:
                st.warning('Yes, the customer will terminate the service :worried:')
                st.write("This customer belongs to Segment",seg)
                if seg in retention_strategies_dict:
                    st.subheader("Retention Strategies for Segment")
                    for strategy in retention_strategies_dict[seg]:
                        st.markdown(f"- {strategy}")
            else:
                st.success('No, the customer is happy with your Services :smiley:')
                
        
        

    else:
        yes=0
        st.subheader("Dataset upload")
        uploaded_file = st.file_uploader("Choose a file")
        if uploaded_file is not None:
            data = pd.read_csv(uploaded_file)
            data.dropna(inplace=True)
            cid = data['customerID'].tolist()
            st.write(data.head())
            st.markdown("<h3></h3>", unsafe_allow_html=True)

            preprocess_df = preprocess(data, "Batch")

            if "predict" not in st.session_state:
                st.session_state.predict = False
            if "Segment" not in st.session_state:
                st.session_state.segment = False
           
            if st.button('Predict'):
                st.session_state.predict = True
                prediction = model.predict(preprocess_df)
                dat1 = np.array(cid)
                dat2 = np.array(prediction)

                prediction_df = pd.DataFrame({'CustomerID': dat1, 'Predictions': dat2}, columns=["CustomerID", "Predictions"])
                prediction_df = prediction_df.replace({1: 'Yes', 0: 'No'})
                dat3 = prediction_df.drop(['CustomerID'], axis=1)
                st.write("Number of customers: ", len(dat3))
                st.write("Number of churn:", len(dat3[dat3['Predictions'] == 'No'].index))
                frames = [data, dat3]
                st.markdown("<h3></h3>", unsafe_allow_html=True)
                st.subheader('Prediction')
                st.write(prediction_df)
                
            
            if st.session_state.predict and st.button("Segment"):
                st.session_state.segment = True
                st.subheader("Customer Segmentation")
                prediction = model.predict(preprocess_df)
                dat1 = np.array(cid)
                dat2 = np.array(prediction)
                

                prediction_df = pd.DataFrame({'CustomerID': dat1, 'Predictions': dat2}, columns=["CustomerID", "Predictions"])
                prediction_df = prediction_df.replace({1: 'Yes', 0: 'No'})
                dat3 = prediction_df.drop(['CustomerID'], axis=1)
                frames = [data, dat3]
                
                dat4 = pd.concat(frames, axis=1)
                dat4.drop(dat4[dat4['Predictions'] == 'No'].index, inplace=True)
                dat4.drop(['Predictions'], axis=1, inplace=True)

                condition_1 = (dat4['tenure'] <= 20)
                condition_2 = dat4['Contract'] != 'Two year'
                condition_3 = ((dat4['Partner'] == 'No') | (dat4['Dependents'] == 0))
                condition_4 = (dat4['InternetService'] == 'Fiber optic')
                condition_5 = ((dat4['TotalCharges'] <= 1000) | (dat4['MonthlyCharges'] > 75))
                condition_6 = dat4['PaymentMethod'] == 'Electronic check'
                condition_7 = (dat4['OnlineSecurity'] == 'No')
                condition_8 = (dat4['TechSupport'] == 'No')
                condition_9 = (dat4['DeviceProtection'] == 'No')
                condition_10 = dat4['OnlineBackup'] == 'No'
                dat4['Segment'] = np.select([condition_1, condition_2, condition_3, condition_4, condition_5, condition_6, condition_7, condition_8, condition_9, condition_10],
                                             [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                                             default=0)
                st.write(dat4)
                segment_counts = dat4['Segment'].value_counts()

                top_segments = segment_counts.head(5)

                fig, ax = plt.subplots()
                ax.pie(top_segments, labels=top_segments.index, autopct='%1.1f%%', startangle=180)
                ax.axis('equal')
                ax.set_title('Top Five Customer Segments', fontsize=16)

                st.pyplot(fig)
                

                
            
                unique_segments = dat4['Segment'].unique()

                st.header("Retention strategies")
                
                for segment in unique_segments:
                    if segment in segment_descriptions:
                        st.subheader(f"Segment {segment}: {segment_descriptions[segment]}")
                    else:
                        st.subheader(f"Segment {segment}")
                    if segment in retention_strategies_dict:
                        strategies = retention_strategies_dict[segment]
                        for strategy in strategies:
                            st.markdown(f"*- {strategy}*")
                    else:
                        st.write("No retention strategies defined for this segment.")

    
if __name__ == '__main__':
    main()
